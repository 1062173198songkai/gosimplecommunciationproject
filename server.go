package main

import (
	"fmt"
	"net"
)

const NETWORK_CONNECT_TYPE string = "tcp4"

type Server struct {
	Ip            string
	Port          int
	OnlineUserMap map[string]*User
}

func NewServer(ip string, port int) *Server {
	return &Server{Ip: ip, Port: port}
}
func (this *Server) handlerConnect(connect net.Conn) {
	fmt.Printf("处理连接:%v", connect.RemoteAddr())
}

func (this *Server) Start() error {
	listen, err := net.Listen(NETWORK_CONNECT_TYPE, fmt.Sprintf("%s:%d", this.Ip, this.Port))
	if err != nil {
		fmt.Printf("创建监听进程异常:%v \r\n", err)
		return err
	} else {
		defer func(listen net.Listener) {
			err := listen.Close()
			if err != nil {
				fmt.Printf("监听进程关闭异常,error:%v, address:%v \r\n", listen.Addr())
			}
		}(listen)

		//创建 accpet 连接
		for {
			connect, err := listen.Accept()
			if err != nil {
				fmt.Printf("创建连接异常,err:%v, client addr:%v, server addr:%v \r\n", err, connect.RemoteAddr(), listen.Addr())
				continue
			}
			go this.handlerConnect(connect)
		}
	}
}
