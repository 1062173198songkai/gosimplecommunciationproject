package main

import "fmt"

func main() {

	var server *Server = &Server{Ip: "localhost", Port: 9898}
	fmt.Printf("Server已创建:%v", *server)
	err := server.Start()
	if err != nil {
		fmt.Printf("创建端口监听失败:%v, error:%v \r\n", *server, err)
		return
	}
	fmt.Printf("创建端口监听成功:%v, error:%v \r\n", *server, err)
}
